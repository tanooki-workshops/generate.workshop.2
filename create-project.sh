#!/bin/bash

set -o allexport; source .env; set +o allexport

curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --form "namespace=$1" \
     --form "path=$2" \
     --form "file=@$3" "https://gitlab.com/api/v4/projects/import"
echo ""

# ./create-project.sh namespace project_name template-file.tar.gz
# ./create-project.sh wasmcooking/suborbital demo  wasmcooking_subo_2022-02-20.tar.gz

# ./create-project.sh tanooki-workshops/vert-x-on-knative/orga_nini_queen hello-world ./samples/wasmcooking_core_2022-02-23.tar.gz
# ./create-project.sh tanooki-workshops/vert-x-on-knative/orga_k33g hello-world ./samples/wasmcooking_core_2022-02-23.tar.gz

