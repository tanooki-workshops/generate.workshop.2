# Generate.workshop

## Create groups

- Set the value of `GITLAB_TOKEN_ADMIN`
- Update/Change the values at the bottom of `A-create-main-group.js`
- Run the below command:
  ```bash
  node A-create-main-group.js
  ```

It will create:
- a subgroup with the name of the hands-on
- 2 sub projects: `communications` (private) and `registrations` (public)

## Generate the hands-on project for every attendee

- Generate en export of the project
- Put it in `samples`


- Update/Change the values at the bottom of `B-create-attendees-groups.js`
- Run the below command:
  ```bash
  node B-create-attendees-groups.js
  ```

It will create:
- a subgroup for every attendee with the appropriate rights
- a batch file (to generate the projects): `create-all-projects.sh`
- type `chmod +x create-all-projects.sh`
- run `./create-all-projects.sh` to create all projects
