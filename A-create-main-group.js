const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper
const ProjectsHelper = require('@tanooki/gl.cli.js/projects.helper').Helper
const IssueHelper = require('@tanooki/gl.cli.js/issues.helper.js').Helper

const RepositoriesHelper = require('@tanooki/gl.cli.js/repositories.helper').Helper
const dedent = require('@tanooki/gl.cli.js').dedent

const registrationsTpl = require('./templates/registrations.readme.js')
const communicationsTpl = require('./templates/communications.readme.js')

const communicationsIssueTpl = require('./templates/communications.issue.js')


/*
  The "inscriptions" project is to provide confidential informations to the attendees
*/
async function create_registrations_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) {
  let subGroup = await GroupsHelper.getSubGroupDetails({subGroup:`${parentGroupName}/${subGroupName}`})
  let subGroupId = subGroup.get("id")
  console.log(subGroupId, subGroup)

  try {
    let project = await ProjectsHelper.createProject({
      name: "registrations",
      path: "registrations",
      nameSpaceId: subGroupId,
      initializeWithReadMe: false,
      visibility: "public"
    })
    console.log(project)

    // +++++ Create README +++++
    try {
      let markdownFile = await RepositoriesHelper.createFile({
        project: `${parentGroupName}/${subGroupName}/registrations`,
        filePath: "README.md",
        branch: "main",
        authorEmail: "pcharriere@gitlab.com",
        content: registrationsTpl.readme({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}),
        commitMessage: "📝 add the README file"
      })
      console.log(markdownFile)
    } catch(error) {
      console.log("😡 creating README [create_registrations_project]", error.message)
      console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }


  } catch(error) {
    console.log("😡 create_registrations_project:", error.message)
    console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

/*
  The "communications" project is to provide confidential informations to the attendees
*/
async function create_communications_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) {
  let subGroup = await GroupsHelper.getSubGroupDetails({subGroup:`${parentGroupName}/${subGroupName}`})
  let subGroupId = subGroup.get("id")
  console.log(subGroupId, subGroup)

  try {
    let project = await ProjectsHelper.createProject({
      name: "communications",
      path: "communications",
      nameSpaceId: subGroupId,
      initializeWithReadMe: false,
      visibility: "private"
    })
    console.log(project)

    // +++++ Create README +++++
    try {
      let markdownFile = await RepositoriesHelper.createFile({
        project: `${parentGroupName}/${subGroupName}/communications`,
        filePath: "README.md",
        branch: "main",
        authorEmail: "pcharriere@gitlab.com",
        content: communicationsTpl.readme({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}),
        commitMessage: "📝 add the README file"
      })
      console.log(markdownFile)
    } catch(error) {
      console.log("😡 creating README [create_communications_project]", error.message)
      console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }

    // +++++ Create a confidential issue +++++
    // It allows to share confidential informations with the attendees

    // communicationsIssueTpl

    let confidentialIssue = await IssueHelper.createIssue({
      project: `${parentGroupName}/${subGroupName}/communications`,
      title: communicationsIssueTpl.title({workShopDescription, workshopDate}),
      description: communicationsIssueTpl.body({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}),
      confidential: true
    })

  } catch(error) {
    console.log("😡 create_communications_project:", error.message)
    console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}


async function create_main_sub_group({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees, codelabPath, landingPagePath}) {

  let parentGroup = await GroupsHelper.getGroupDetails({group:parentGroupName})
  let parent_group_id = parentGroup.get("id")
  console.log(parent_group_id)

  try {
    let workShopGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: subGroupName,
      subGroupName: subGroupName,
      visibility: "public"
    })
    console.log("📝", workShopGroup)

    // *** create inscriptions project ***
    await create_registrations_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})

    // *** create communications project ***
    await create_communications_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})

  } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }

}

// YOU NEED TO SET `GITLAB_TOKEN_ADMIN`
create_main_sub_group({
  parentGroupName: "tanooki-workshops",
  subGroupName: "vert-x-on-knative", // workshop name
  workShopDescription: "Deploy Vert-x Microservice on Knative with GitLab CI",
  workshopDate: "Tuesday, March 8th - 07:00 PM",
  maxAttendees: 10,
  codelabPath: "🚧", // Location of the codelab project
  landingPagePath: "🚧" // Location of the landing-page project
})

