let readme = ({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) =>`# ${subGroupName} Communications

- **Description**: 🧰 **${workShopDescription}**
- **Date**: 🖐️ ${workshopDate}
- **Every attendees group is located here**: [https://gitlab.com/${parentGroupName}/${subGroupName}](https://gitlab.com/${parentGroupName}/${subGroupName})
- **All steps/Exercises**: 👣 there is an **exercises** project in every attendee's group: each issue of the project is a step of the workshop
- **Meeting link**: [Insert Link](insert link)

`

module.exports = {
  readme: readme
}


