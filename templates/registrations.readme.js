// registrations.readme.js
let readme = ({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) =>`# ${subGroupName} registrations

- **Topic**: ${workShopDescription}
- **Date**: ${workshopDate}

## How to register to the workshop?

It's simple: you just need to create an issue in this project.

> the ${maxAttendees} first created issues will be kept

`

module.exports = {
  readme: readme
}

