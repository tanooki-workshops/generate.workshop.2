// 🚧 WIP
//node create-attendees-groups "$PARENT_GROUP_NAME" "$WORKSHOP_NAME" $MAX_ATTENDEES "$HANDSON_PROJECT_PATH" "$HANDSON_PROJECT_TARGET_NAME"
const spawn = require('child_process').spawn
const fs = require('fs')

const IssueHelper = require('@tanooki/gl.cli.js/issues.helper.js').Helper
const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper
const ProjectsHelper = require('@tanooki/gl.cli.js/projects.helper').Helper

const groupAttendeesPrefix = "grp"
//const groupAttendeesPrefix = "orga"


async function create_attendees_groups_from_registrations_issues({parentGroupName, subGroupName, maxAttendees, projectName, projectTemplate}) {
  console.log("🎃 maxAttendees -1", (maxAttendees-1))


  let parentGroup = await GroupsHelper.getGroupDetails({group:`${parentGroupName}/${subGroupName}`})
  let parent_group_id = parentGroup.get("id")
  console.log("👋", parent_group_id)

  let issues = await IssueHelper.getProjectIssues({project: `${parentGroupName}/${subGroupName}/registrations`})
  let firstTenIssues = issues.reverse().slice(0, (maxAttendees-1))

  let batch_lines = []
  batch_lines.push("#!/bin/bash")
  batch_lines.push("")

  // Create a group for every attendee
  for (var member in firstTenIssues) {
    let issue = firstTenIssues[member]
    console.log("✅", issue.get("iid"), issue.get("id"), issue.get("author").username)

    let line = `
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN_ADMIN" \
--form "namespace=${parentGroupName}/${subGroupName}/${groupAttendeesPrefix}_${issue.get("author").username}" \
--form "path=${projectName}" \
--form "file=@${projectTemplate}" "https://gitlab.com/api/v4/projects/import"
            `
    
    batch_lines.push(line)
    batch_lines.push("")

    // 🖐️ here, add every subscribed member to the communications project.
    try {
      let projectMember = await ProjectsHelper.addMemberToProject({
        project: `${parentGroupName}/${subGroupName}/communications`,
        userId: issue.get("author").id,
        accessLevel: 30 // dev access
      })
      console.log(projectMember)
    } catch(error) {
      console.log("😡 add member to communications project [create_attendees_groups_from_registrations_issues]:", error.message)
      console.log("🖐️ parameters:", {parentGroupName, subGroupName, maxAttendees, projectName, projectTemplate})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }


    try {
      // Create a group for the current attendee
      let attendeeGroup = await GroupsHelper.createSubGroup({
        parentGroupId: parent_group_id,
        subGroupPath: `${groupAttendeesPrefix}_${issue.get("author").username}`,
        subGroupName: `${groupAttendeesPrefix}_${issue.get("author").username}`,
        visibility: "public"
      })
      console.log("📝", attendeeGroup)

      // Add the current attendee as the owner of the group
      try {
        let groupMember = await GroupsHelper.addMemberToGroup({
          group: `${parentGroupName}/${subGroupName}/${groupAttendeesPrefix}_${issue.get("author").username}`,
          userId: issue.get("author").id,
          accessLevel: 50 // owner access
        })
        console.log("😃", groupMember.username)

      } catch(error) {
        console.log("😡 add projects to the group of the attendee [create_attendees_groups_from_registrations_issues]:", error.message)
        console.log("🖐️ parameters:", {parentGroupName, subGroupName, maxAttendees, projectName, projectTemplate})
        console.log(error.response.status, error.response.statusText)
        console.log(error.response.data)
      }

    } catch(error) {
        console.log("😡 create the group of the attendee [create_attendees_groups_from_registrations_issues]:", error.message)
        console.log("🖐️ parameters:", {parentGroupName, subGroupName, maxAttendees, projectName, projectTemplate})
        console.log(error.response.status, error.response.statusText)
        console.log(error.response.data)
    }

  }
  
  fs.writeFileSync("create-all-projects.sh", batch_lines.join("\n"));

}


create_attendees_groups_from_registrations_issues({
  parentGroupName: "tanooki-workshops",
  subGroupName: "vert-x-on-knative", // workshop name
  maxAttendees: 10,
  projectName: "hello-world",
  projectTemplate: "./samples/test-01.tar.gz"
})



